// let trainer = {
// 	name: "Ash Ketchum",
// 	age: 10,
// 	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],


	function trainer(name, age, pokemon, friends){
		this.name = name;
		this.age = age;
		this.pokemon = pokemon;
		this.friends = friends;
	}

	let myTrainer = new trainer();


	myTrainer.name = "Ash ketchum";
	myTrainer.age = 10,
	myTrainer.pokemon = ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"];
	myTrainer.friends = {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"],
	
	};
	console.log(myTrainer);
	console.log("Result dot notation:");
	console.log(myTrainer.name);
	console.log("Result of square bracket notation:")
	console.log(myTrainer['pokemon']);

	let person = {
	    name: "Pikachu",
	    talk: function(){
	        console.log(this.name + "! I choose you!");
	    }
	    
	}

	console.log(person);
    person.talk();


    function Pokemon(name, level){
        this.name = name;
        this.level = level;
        this.health = level*2;
        this.attack = level;


            this.tackle = function(target){
                console.log(this.name + ' tackled ' + target.name);
                target.health -= this.attack;
                console.log(target.name+" health is now reduced to "+ target.health)


                if(target.health <= 0){ 
                    target.faint()
                }

            }
            this.faint = function(){
                console.log(this.name + " fainted");
            }

        }

        let pikachu = new Pokemon("Pikachu ", 12);
        console.log(pikachu);

        let geodude = new Pokemon("Geodude", 8);
        console.log(geodude);

        let mewtwo = new Pokemon("Mewtwo", 100);
        console.log(mewtwo);

        geodude.tackle(pikachu);


        pikachu.health = 16;
        console.log(pikachu);

        mewtwo.tackle(geodude);

        geodude.health = -84;
        console.log(geodude);